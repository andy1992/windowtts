# README #

An simple command line program that synthesize input text into speech using microsoft speech API.


## Set up ##

1. GO to: https://drive.google.com/file/d/0B4xqb3dimLD_Z0ZPWjFubGxab0E/view?usp=sharing Download and unzip the rar file.
2. Install SpeechPlatformRuntime(32bit) if your computer does not already have it.
3. Run the command "TTS.exe -l" to list the languages installed on your computer.
4. If the language/voice you want is not available, go to: https://www.microsoft.com/en-us/download/details.aspx?id=27224
Pick the voice you are interested in(**Be sure to pick the file with "TTS" as the ones without it are speech recognition files**). Download and install the runtime language and it should be available for use immediately. This program should also be compatible with 3rd party runtime language that can utilize Microsoft Speech API.


## Usage ##

### Examples ###

To see list of available commands.
```
#!
>TTS.exe

```


List all available languages.
```
#!
>TTS.exe -l
```


Use Microsoft Anna to speak something.
```
#!
>TTS.exe "Microsoft Anna" "How are you?"
```


Use Microsoft Anna to speak and direct output to file using -o argument.
```
#!
>TTS.exe "Microsoft Anna" "How are you?" -o out.mp3
```


Use Microsoft Anna to read from input file using -i argument and direct output to file in wav format. 16000 bit per sample, 8 Audio bit per sample, mono audio channel.
```
#!
>TTS.exe "Microsoft Anna" -i input.txt -o out.wav -wav 16000_8_Mono
```


You can change the rate at which it speak using -rate argument. -10 being the slowest and 10 being the fastest.
```
#!
>TTS.exe "Microsoft Anna" "I can talk very fast" -rate 10
```


You can replace the text from input with something else by using the -replace argument with a text file that indicate what is to be replaced with what. The file is read like a CSV where strings in the first column is replaced with strings in the second column. The order of which strings are replaced is ordered by the length of strings in the first column in descending order to decrease the chance of conflict. Replacement.txt must be encoded in utf8.
Inside Replacement.txt:
```
#!
google,microsoft
is not working,is working
```
Calling:
```
#!
>TTS.exe "Microsoft Anna" "google tts scripts is not working" -replace Replacement.txt
```
Will say: "microsoft tts scripts is working".


## Note ##

* By default, output audio files are mp3 format. Use the -wav argument to output to wav format.
* If the spoken text does not come out correctly, this might be due to the encoding of the input text file. By default, the program picks the encoding commonly used with the selected language/voice. If you know that the input file is encoded in utf8, use the -utf8 argument.


## Final Thoughts ##
As it turns out, there are two speech synthesis program Microsoft provides. One is Microsoft.Speech.Synthesis and other is System.Speech.Synthesis. Both of them require the exact same function calls but are two different namespace. The Microsoft one uses Speech Platform Runtime version 11.0 while 3rd party TTS tends to use the System one. This program uses both the microsoft and system speech synthesis so that it can use more voices.